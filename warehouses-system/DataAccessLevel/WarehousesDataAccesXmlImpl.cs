﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Xml.Linq;

namespace warehouses_system
{
    public class WarehousesDataAccesXmlImpl : WarehousesDataAccess
    {
        private static WarehousesDataAccesXmlImpl instance;
        private static readonly string path = "database.xml";
        private XDocument db;

        private WarehousesDataAccesXmlImpl()
        {
            instance = this;
            try
            {
                db = XDocument.Load(path);
            }
            catch(FileNotFoundException)
            {
                db = new XDocument(new XElement("warehouse-system",
                    new XElement("warehouses"),
                    new XElement("products"),
                    new XElement("productTypes")
                    ));
                db.Save(path);
            }
        }

        public static WarehousesDataAccesXmlImpl GetInstance()
        {
            if(instance == null)
                return new WarehousesDataAccesXmlImpl();
            return instance;
        }

        public IEnumerable<ProductType> GetAllProductTypes()
        {
            return from e in db.Descendants("warehouse-system").Elements("productTypes").Elements("productType")
                   select new ProductType(e.Attribute("name").Value, e.Attribute("fabricator").Value,
                       e.Attribute("units").Value, Convert.ToInt32(e.Attribute("defaultPrice").Value));
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return from e in db.Descendants("warehouse-system").Elements("products").Elements("product")
                   select new Product(Convert.ToInt32(e.Attribute("id").Value), GetProductType(e.Attribute("productType").Value),
                           Convert.ToInt32(e.Attribute("count").Value), Convert.ToInt32(e.Attribute("price").Value));
        }

        public IEnumerable<Warehouse> GetAllWarehouses()
        {
            var result = new List<Warehouse>();
            foreach(var e in db.Descendants("warehouse-system").Elements("warehouses").Elements("warehouse"))
            {
                var products = from p in e.Elements("products").Elements("product")
                               select GetProduct(Convert.ToInt32(p.Attribute("id").Value));
                var types = from t in e.Elements("supportTypes").Elements("productType")
                            select GetProductType(t.Attribute("name").Value);
                result.Add(new Warehouse(Convert.ToInt32(e.Attribute("id").Value), types));
                foreach(var product in products)
                    result.Last().addProduct(product);
            }
            return result;
        }

        public void Remove(Warehouse warehouse)
        {
            var result = ( from e in db.Descendants("warehouse")
                           where e.Attribute("id").Value.Equals(warehouse.Id.ToString())
                           select e );
            if(result != null)
                result.Remove();
            db.Save(path);
        }

        public void Remove(ProductType productType)
        {
            var result = from e in db.Descendants("productType")
                         where e.Attribute("name").Value.Equals(productType.Name)
                         select e;
            foreach(var e in result)
                e.Remove();
            db.Save(path);
        }

        public void Remove(Product product)
        {
            var result = from e in db.Descendants("product")
                         where e.Attribute("id").Value.Equals(product.Id.ToString())
                         select e;
            foreach(var e in result)
                e.Remove();
            db.Save(path);
        }

        public void Update(Warehouse warehouse)
        {
            foreach(var type in warehouse.SupportTypes)
                Update(type);
            foreach(var product in warehouse.getProducts())
                Update(product);
            var result = from e in db.Descendants("warehouse")
                         where e.Attribute("id").Value.Equals(warehouse.Id.ToString())
                         select e;
            if(result.Count() > 0)
                result.Remove();
            var eproducts = new XElement("products");
            foreach(var product in warehouse.getProducts())
                eproducts.Add(new XElement("product", new XAttribute("id", product.Id)));
            var etypes = new XElement("supportTypes");
            foreach(var type in warehouse.SupportTypes)
                etypes.Add(new XElement("productType", new XAttribute("name", type.Name)));
            db.Descendants("warehouses").First().Add(new XElement("warehouse",
                new XAttribute("id", warehouse.Id),
                eproducts,
                etypes
                ));
            db.Save(path);
        }

        public void Update(ProductType productType)
        {
            var result = from e in db.Descendants("productType")
                         where e.Attribute("name").Value.Equals(productType.Name)
                         select e;
            if(result.Count() == 0)
                db.Descendants("productTypes").First().Add(new XElement("productType",
                    new XAttribute("name", productType.Name),
                    new XAttribute("fabricator", productType.Fabricator),
                    new XAttribute("units", productType.Units),
                    new XAttribute("defaultPrice", productType.DefaultPrice)
                    ));
            else
                foreach(var e in result)
                    e.ReplaceAttributes(
                        new XAttribute("name", productType.Name),
                        new XAttribute("fabricator", productType.Fabricator),
                        new XAttribute("units", productType.Units),
                        new XAttribute("defaultPrice", productType.DefaultPrice)
                        );
            db.Save(path);
        }

        public void Update(Product product)
        {
            Update(product.Type);
            var result = from e in db.Descendants("product")
                         where e.Attribute("id").Value.Equals(product.Id.ToString())
                         select e;
            if(result.Count() == 0)
                db.Descendants("products").First().Add(new XElement("product",
                    new XAttribute("id", product.Id),
                    new XAttribute("count", product.Count),
                    new XAttribute("productType", product.Type.Name),
                    new XAttribute("price", product.Price)
                    ));
            else
                foreach(var e in result)
                    e.ReplaceAttributes(new XAttribute("id", product.Id),
                        new XAttribute("count", product.Count),
                        new XAttribute("productType", product.Type.Name),
                        new XAttribute("price", product.Price)
                        );
            db.Save(path);
        }

        public void ClearWarehouses()
        {
            foreach(var e in db.Descendants("warehouse"))
                e.Remove();
            db.Save(path);
        }

        public void ClearProductTypes()
        {
            foreach(var e in db.Descendants("productType"))
                e.Remove();
            db.Save(path);
        }

        public void ClearProducts()
        {
            foreach(var e in db.Descendants("product"))
                e.Remove();
            db.Save(path);
        }

        public void Clear()
        {
            db = new XDocument(new XElement("warehouse-system",
                    new XElement("warehouses"),
                    new XElement("products"),
                    new XElement("productTypes")
                    ));
            db.Save(path);
        }

        public ProductType GetProductType(string name)
        {
            return ( from e in db.Descendants("warehouse-system").Elements("productTypes").Elements("productType")
                     where e.Attribute("name").Value.Equals(name)
                     select new ProductType(e.Attribute("name").Value, e.Attribute("fabricator").Value,
                         e.Attribute("units").Value, Convert.ToInt32(e.Attribute("defaultPrice").Value)) ).First();
        }

        public Product GetProduct(int id)
        {
            var result = from e in db.Descendants("warehouse-system").Elements("products").Elements("product")
                         where e.Attribute("id").Value.Equals(id.ToString())
                         select
                             new Product(Convert.ToInt32(e.Attribute("id").Value),
                                 GetProductType(e.Attribute("productType").Value),
                                 Convert.ToInt32(e.Attribute("count").Value), Convert.ToInt32(e.Attribute("price").Value));
            if(result.Count() > 0)
                return result.First();
            else
                return null;
        }

        public Warehouse GetWarehouse(int id)
        {
            Warehouse result = null;
            foreach(var e in db.Descendants("warehouses").Elements("warehouse"))
            {
                if(!e.Attribute("id").Value.Equals(id.ToString()))
                    continue;
                var products = from p in e.Elements("products").Elements("product")
                               select GetProduct(Convert.ToInt32(p.Attribute("id").Value));
                var types = from t in e.Elements("supportTypes").Elements("productType")
                            select GetProductType(t.Attribute("name").Value);
                result = new Warehouse(Convert.ToInt32(e.Attribute("id").Value), types);
                foreach(var product in products)
                    result.addProduct(product);
            }
            return result;
        }
    }
}
