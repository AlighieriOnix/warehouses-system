﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace warehouses_system
{
    public interface WarehousesDataAccess
    {
        IEnumerable<Product> GetAllProducts();

        IEnumerable<Warehouse> GetAllWarehouses();

        IEnumerable<ProductType> GetAllProductTypes();

        void Remove(Warehouse warehouse);

        void Remove(ProductType productType);

        void Remove(Product product);

        void Update(Warehouse warehouse);

        void Update(ProductType productType);

        void Update(Product product);

        void ClearWarehouses();

        void ClearProductTypes();

        void ClearProducts();

        void Clear();

        ProductType GetProductType(string name);

        Product GetProduct(int id);

        Warehouse GetWarehouse(int id);
    }
}
