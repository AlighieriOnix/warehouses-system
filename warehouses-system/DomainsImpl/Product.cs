﻿using System.Collections.Generic;

namespace warehouses_system
{
    public class ProductType
    {
        public ProductType(string name, string fabricator, string units, int defaultPrice)
        {
            Name = name;
            Fabricator = fabricator;
            Units = units;
            DefaultPrice = defaultPrice;
        }

        public readonly string Name;
        public readonly string Fabricator;
        public readonly string Units;
        public readonly int DefaultPrice;
    }

    public class Product
    {
        public Product(int id, ProductType type, int count, int price)
        {
            Id = id;
            Type = type;
            Count = count;
            Price = price;
        }

        public Product(int id, ProductType type, int count)
            : this(id, type, count, type.DefaultPrice)
        {

        }

        public readonly int Id;
        public readonly int Count;
        public readonly int Price;
        public readonly ProductType Type;

        private Warehouse _warehouse;
        public Warehouse Warehouse
        {
            get
            {
                return _warehouse;
            }
            set
            {
                if(_warehouse != null)
                    _warehouse.removeProduct(this);
                _warehouse = value;
                _warehouse.addProduct(this);
            }
        }
    }
}
