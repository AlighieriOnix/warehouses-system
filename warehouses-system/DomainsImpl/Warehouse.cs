﻿using System.Collections.Generic;
using System.Linq;
namespace warehouses_system
{
   public class Warehouse
    {
        public Warehouse(int id, IEnumerable<ProductType> supportTypes)
        {
            SupportTypes = supportTypes.ToArray();
            Id = id;
        }

        public readonly int Id;
        public readonly ProductType[] SupportTypes;
        private readonly List<Product> products = new List<Product>();

        public bool checkProduct(Product product)
        {
            return SupportTypes.Contains(product.Type);
        }

        public List<Product> getProducts()
        {
            return new List<Product>(products);
        }

        public void addProduct(Product product)
        {
            if(checkProduct(product))
                products.Add(product);
        }

        public void removeProduct(Product product)
        {
            products.Remove(product);
        }
    }
}
