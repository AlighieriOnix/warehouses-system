﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using warehouses_system;

namespace warehouses_system
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataAccess = WarehousesDataAccesXmlImpl.GetInstance();
            var type1 = new ProductType("сало", "вишенвский", "кг", 4);
            var type2 = new ProductType("пиво", "второй", "литр", 10);
            dataAccess.Update(type1);
            dataAccess.Update(type2);
            var p1 = new Product(4, type1, 24);
            var p2 = new Product(5, type2, 56);
            dataAccess.Update(p1);
            dataAccess.Update(p2);
            var l1 = new List<ProductType>();
            l1.Add(type1);
            var l2 = new List<ProductType>();
            l2.Add(type2);
            var w1 = new Warehouse(1, l1);
            var w2 = new Warehouse(2, l2);
            w1.addProduct(p1);
            w2.addProduct(p2);
            dataAccess.Update(w1);
            dataAccess.Update(w2);
            refreshProductTypes();
            refreshProducts();
            refreshWarehouses();
        }

        private WarehousesDataAccess dataAccess;
        private IEnumerable<ProductType> productTypes;
        private IEnumerable<Product> products;
        private IEnumerable<Warehouse> warehouses;

        private void refreshProductTypes()
        {
            productTypes = dataAccess.GetAllProductTypes();
            foreach(var productType in productTypes)
                productTypesGrid.Rows.Add(productType.Name, productType.Fabricator, productType.Units,
                    productType.DefaultPrice);
        }

        private void refreshProducts()
        {
            products = dataAccess.GetAllProducts();
            var types = from type in productTypes
                        select type.Name;
            ( productsGrid.Columns[1] as DataGridViewComboBoxColumn ).DataSource = new List<String>(types);
            foreach(var product in products)
                productsGrid.Rows.Add(product.Id, product.Type.Name, product.Count, product.Price);
        }

        private void refreshWarehouses()
        {
            warehouses = dataAccess.GetAllWarehouses();
            var prs = from p in products
                      select p.Id.ToString();
            ( warehousesProductsGrid.Columns[0] as DataGridViewComboBoxColumn ).DataSource = new List<String>(prs);
            var types = from type in productTypes
                        select type.Name;
            ( warehousesTypesGrid.Columns[0] as DataGridViewComboBoxColumn ).DataSource = new List<String>(types);
            foreach(var warehouse in warehouses)
                warehousesIdGrid.Rows.Add(warehouse.Id.ToString());
        }


        private void refresh()
        {

        }

        private void productTypesGrid_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {

        }

        private void warehousesIdGrid_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                var warehouse = dataAccess.GetWarehouse(Convert.ToInt32(warehousesIdGrid.SelectedRows[0].Cells[0].Value));
                warehousesTypesGrid.Rows.Clear();
                warehousesProductsGrid.Rows.Clear();
                foreach (var type in warehouse.SupportTypes)
                    warehousesTypesGrid.Rows.Add(type.Name);
                foreach(var product in warehouse.getProducts())
                    warehousesProductsGrid.Rows.Add(product.Id.ToString());
            }
            catch(Exception d)
            {

            }
        }
    }
}
