﻿namespace warehouses_system
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && ( components != null ))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.warehousesProductsGrid = new System.Windows.Forms.DataGridView();
            this.warehousesProducts = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.warehousesTypesGrid = new System.Windows.Forms.DataGridView();
            this.supportType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.warehousesIdGrid = new System.Windows.Forms.DataGridView();
            this.warehouse_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.productsGrid = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.productTypesGrid = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fabricator = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.units = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.defaultPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.warehousesProductsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.warehousesTypesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.warehousesIdGrid)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsGrid)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productTypesGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(807, 506);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.warehousesProductsGrid);
            this.tabPage1.Controls.Add(this.warehousesTypesGrid);
            this.tabPage1.Controls.Add(this.warehousesIdGrid);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(799, 480);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Warehouses";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // warehousesProductsGrid
            // 
            this.warehousesProductsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.warehousesProductsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.warehousesProducts});
            this.warehousesProductsGrid.Location = new System.Drawing.Point(489, 6);
            this.warehousesProductsGrid.Name = "warehousesProductsGrid";
            this.warehousesProductsGrid.Size = new System.Drawing.Size(304, 468);
            this.warehousesProductsGrid.TabIndex = 2;
            // 
            // warehousesProducts
            // 
            this.warehousesProducts.HeaderText = "products";
            this.warehousesProducts.Name = "warehousesProducts";
            // 
            // warehousesTypesGrid
            // 
            this.warehousesTypesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.warehousesTypesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.supportType});
            this.warehousesTypesGrid.Location = new System.Drawing.Point(175, 6);
            this.warehousesTypesGrid.Name = "warehousesTypesGrid";
            this.warehousesTypesGrid.Size = new System.Drawing.Size(308, 468);
            this.warehousesTypesGrid.TabIndex = 1;
            // 
            // supportType
            // 
            this.supportType.HeaderText = "support types";
            this.supportType.Name = "supportType";
            this.supportType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.supportType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // warehousesIdGrid
            // 
            this.warehousesIdGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.warehousesIdGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.warehouse_id});
            this.warehousesIdGrid.Location = new System.Drawing.Point(6, 6);
            this.warehousesIdGrid.Name = "warehousesIdGrid";
            this.warehousesIdGrid.Size = new System.Drawing.Size(163, 468);
            this.warehousesIdGrid.TabIndex = 0;
            this.warehousesIdGrid.SelectionChanged += new System.EventHandler(this.warehousesIdGrid_SelectionChanged);
            // 
            // warehouse_id
            // 
            this.warehouse_id.HeaderText = "id";
            this.warehouse_id.Name = "warehouse_id";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.productsGrid);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(799, 480);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Products";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // productsGrid
            // 
            this.productsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.type,
            this.count,
            this.price});
            this.productsGrid.Location = new System.Drawing.Point(6, 6);
            this.productsGrid.Name = "productsGrid";
            this.productsGrid.Size = new System.Drawing.Size(787, 468);
            this.productsGrid.TabIndex = 0;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // type
            // 
            this.type.HeaderText = "type";
            this.type.Name = "type";
            // 
            // count
            // 
            this.count.HeaderText = "count";
            this.count.Name = "count";
            // 
            // price
            // 
            this.price.HeaderText = "price";
            this.price.Name = "price";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.productTypesGrid);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(799, 480);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ProductTypes";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(531, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // productTypesGrid
            // 
            this.productTypesGrid.AllowDrop = true;
            this.productTypesGrid.AllowUserToOrderColumns = true;
            this.productTypesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productTypesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.fabricator,
            this.units,
            this.defaultPrice});
            this.productTypesGrid.Location = new System.Drawing.Point(3, 3);
            this.productTypesGrid.Name = "productTypesGrid";
            this.productTypesGrid.Size = new System.Drawing.Size(506, 474);
            this.productTypesGrid.TabIndex = 0;
            this.productTypesGrid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.productTypesGrid_UserDeletingRow);
            // 
            // name
            // 
            this.name.HeaderText = "name";
            this.name.Name = "name";
            // 
            // fabricator
            // 
            this.fabricator.HeaderText = "fabricator";
            this.fabricator.Name = "fabricator";
            // 
            // units
            // 
            this.units.HeaderText = "units";
            this.units.Name = "units";
            // 
            // defaultPrice
            // 
            this.defaultPrice.HeaderText = "defaultPrice";
            this.defaultPrice.Name = "defaultPrice";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 541);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.warehousesProductsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.warehousesTypesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.warehousesIdGrid)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.productsGrid)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productTypesGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView productTypesGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView productsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewComboBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fabricator;
        private System.Windows.Forms.DataGridViewTextBoxColumn units;
        private System.Windows.Forms.DataGridViewTextBoxColumn defaultPrice;
        private System.Windows.Forms.DataGridView warehousesProductsGrid;
        private System.Windows.Forms.DataGridViewComboBoxColumn warehousesProducts;
        private System.Windows.Forms.DataGridView warehousesTypesGrid;
        private System.Windows.Forms.DataGridViewComboBoxColumn supportType;
        private System.Windows.Forms.DataGridView warehousesIdGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn warehouse_id;
    }
}

